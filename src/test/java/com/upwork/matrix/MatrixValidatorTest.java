package com.upwork.matrix;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MatrixValidatorTest {

    private MatrixValidator validator = new MatrixValidator();

    private int[][] validMatrixA = new int[][]{{1, -1}, {2, 0}, {3, 0}};
    private int[][] validMatrixB = new int[][]{{1, 1}, {2, 0}};

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void shouldNotThrowException_whenInvokeValidateNotEmpty_givenValidMatrix() {
        validator.validateNotEmpty(validMatrixA);
    }

    @Test
    public void shouldNotThrowException_whenInvokeValidateSymmetry_givenValidMatrix() {
        validator.validateSymmetry(validMatrixA);
    }

    @Test
    public void shouldNotThrowException_whenInvokeValidateMultiplyingPossibility_givenValidMatrices() {
        validator.validateMultiplyingPossibility(validMatrixA, validMatrixB);
    }

    @Test
    public void shouldThrowException_whenInvokeValidateNotEmpty_givenNullMatrix() {
        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrix is empty: 'null'");

        validator.validateNotEmpty(null);
    }

    @Test
    public void shouldThrowException_whenInvokeValidateSymmetry_givenEmptyMatrix() {
        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrix is empty: '[]'");

        validator.validateNotEmpty(new int[][]{});
    }

    @Test
    public void shouldThrowException_whenInvokeValidateSymmetry_givenAsymmetricalMatrix() {
        int[][] asymmetricalMatrix = new int[][]{{1, 2}, {1}};

        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrix is not symmetric: '[[1, 2], [1]]'");

        validator.validateSymmetry(asymmetricalMatrix);
    }

    @Test
    public void shouldThrowException_whenInvokeMultiplyingPossibility_givenNotMultiplyingMatrices() {
        int[][] a = new int[][]{{1, 2, 4}, {3, 4, -3}};
        int[][] b = new int[][]{{1, 2}, {5, -7}};

        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrices is not consistent, cannot be multiplied");

        validator.validateMultiplyingPossibility(a, b);
    }

}
