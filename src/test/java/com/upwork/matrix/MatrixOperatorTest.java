package com.upwork.matrix;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MatrixOperatorTest {

    @Mock
    private MatrixValidator validator;

    @InjectMocks
    private MatrixOperator matrixOperator = new MatrixOperator();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private int[][] validMatrixA = new int[][]{{1, -1}, {2, 0}, {3, 0}};
    private int[][] validMatrixB = new int[][]{{1, 1}, {2, 0}};

    @Test
    public void shouldReturnRightCalculationResult_whenInvokeMultiply_givenValidMatrices() {
        int[][] expected = {{-1, 1}, {2, 2}, {3, 3}};
        int[][] result = matrixOperator.multiply(validMatrixA, validMatrixB);

        assertArrayEquals(expected, result);
    }

    @Test
    public void shouldFail_whenInvokeMultiply_givenNotEmptyValidationFails() {
        doThrow(new MatrixValidationException("Matrix is empty: '[]'")).when(validator).validateNotEmpty(any());
        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrix is empty: '[]'");

        matrixOperator.multiply(validMatrixA, validMatrixB);
    }

    @Test
    public void shouldFail_whenInvokeMultiply_givenSymmetryValidationFails() {
        doThrow(new MatrixValidationException("Matrix is not symmetric: '[[1, 2], [1]]'")).when(validator).validateSymmetry(any());
        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrix is not symmetric: '[[1, 2], [1]]'");

        matrixOperator.multiply(validMatrixA, validMatrixB);
    }

    @Test
    public void shouldFail_whenInvokeMultiply_givenMultiplyingPossibilityValidationFails() {
        MatrixValidationException ex = new MatrixValidationException("Matrices is not consistent, cannot be multiplied");
        doThrow(ex).when(validator).validateMultiplyingPossibility(any(), any());
        exception.expect(MatrixValidationException.class);
        exception.expectMessage("Matrices is not consistent, cannot be multiplied");

        matrixOperator.multiply(validMatrixA, validMatrixB);
    }

    @Test
    public void shouldInvokeValidators_whenInvokeMultiply_givenValidMatrices() {
        matrixOperator.multiply(validMatrixA, validMatrixB);

        verify(validator, times(1)).validateNotEmpty(validMatrixA);
        verify(validator, times(1)).validateNotEmpty(validMatrixB);
        verify(validator, times(1)).validateSymmetry(validMatrixA);
        verify(validator, times(1)).validateSymmetry(validMatrixB);
        verify(validator, times(1)).validateMultiplyingPossibility(validMatrixA, validMatrixB);
    }

}
