package com.upwork.matrix;

public class MatrixOperator {

    private MatrixValidator matrixValidator;

    public MatrixOperator() {
        this.matrixValidator = new MatrixValidator();
    }

    public int[][] multiply(int[][] a, int[][] b) {
        validateMatrices(a, b);
        int aHeight = a.length;
        int bWidth = b[0].length;
        int[][] result = new int[aHeight][bWidth];

        for (int i = 0; i < aHeight; i++) {
            for (int j = 0; j < bWidth; j++) {
                for (int k = 0; k < b.length; k++) {
                    result[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return result;
    }

    private void validateMatrices(int[][] a, int[][] b) {
        matrixValidator.validateNotEmpty(a);
        matrixValidator.validateNotEmpty(b);
        matrixValidator.validateSymmetry(a);
        matrixValidator.validateSymmetry(b);
        matrixValidator.validateMultiplyingPossibility(a, b);
    }

}
