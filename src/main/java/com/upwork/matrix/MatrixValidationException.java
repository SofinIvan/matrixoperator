package com.upwork.matrix;

public class MatrixValidationException extends RuntimeException {

    public MatrixValidationException() {
    }

    public MatrixValidationException(String message) {
        super(message);
    }

    public MatrixValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
