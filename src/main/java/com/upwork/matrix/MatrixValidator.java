package com.upwork.matrix;

import static java.util.Arrays.deepToString;

public class MatrixValidator {

    public void validateMultiplyingPossibility(int[][] a, int[][] b) {
        if (a[0].length != b.length) {
            throw new MatrixValidationException("Matrices is not consistent, cannot be multiplied");
        }
    }

    public void validateNotEmpty(int[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            throwMatrixValidationException("Matrix is empty", matrix);
        }
    }

    public void validateSymmetry(int[][] matrix) {
        int basicLength = matrix[0].length;
        for (int i = 1; i < matrix.length; i++) {
            if (matrix[i].length != basicLength) {
                throwMatrixValidationException("Matrix is not symmetric", matrix);
            }
        }
    }

    private void throwMatrixValidationException(String message, int[][] matrix) {
        String msg = String.format("%s: '%s'", message, deepToString(matrix));
        throw new MatrixValidationException(msg);
    }

}
