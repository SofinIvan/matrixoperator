package com.upwork.matrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Starter {

    public static void main(String[] args) throws IOException {
        System.out.println("Matrix calculator (for multiplying matrices) started");

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String firstMatrixName = "first";
            String secondMatrixName = "second";

            int[][] a = readMatrixFromConsole(firstMatrixName, reader);
            int[][] b = readMatrixFromConsole(secondMatrixName, reader);

            printMatrix(new MatrixOperator().multiply(a, b));
        }
    }

    private static int[][] readMatrixFromConsole(String matrixName, BufferedReader reader) throws IOException {
        System.out.println(String.format("Enter %s matrix rows: ", matrixName));
        int rows = Integer.parseInt(reader.readLine());
        System.out.println(String.format("Enter %s matrix columns: ", matrixName));
        int columns = Integer.parseInt(reader.readLine());

        int[][] matrix = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            String message = "Enter %s matrix row %d (%d whole number(s), separated by spaces): %n";
            System.out.print(String.format(message, matrixName, i + 1, columns));

            matrix[i] = Arrays.stream(reader.readLine().split(" "))
                    .mapToInt(Integer::parseInt)
                    .toArray();
        }
        return matrix;
    }

    private static void printMatrix(int[][] result) {
        System.out.println("Calculation result: ");
        for (int[] ints : result) {
            System.out.println(Arrays.toString(ints));
        }
    }

}
