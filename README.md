##Service for matrix multiplying

###1. What this service does?
 - It multiplies two matrices from whole numbers

###2. What should I do to multiply matrices?
 Enter from command line size of the matrices (rows and columns) and then enter elements of the matrices
 
###3. How to start this application?
 - Download sources
 
 - Open console and go to the project directory
 
 - Make executable jar file with command: **mvn package**   (maven should be installed on your machine)
 
 - Start application using command: **java -jar target/matrix-1.0.jar** (java should be installed on your machine)


**April, 2019**

**i.sofin@gmail.com**